import flask_restplus
from flask import Blueprint

from console.api.environ import push_environ_to_g


api_blueprint_v1 = Blueprint('console api' + '_v1', __name__)


api = flask_restplus.Api(
    app=api_blueprint_v1,
    version='1.0',
    title='Vedavaapi Console Companion Server',
    doc='/docs'
)


api_blueprint_v1.before_request(push_environ_to_g)


from . import auth_ns
