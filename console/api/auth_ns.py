import json
import os
import time

import flask
import requests
from flask import g
import flask_restplus
from furl import furl
from requests import HTTPError

from . import api

auth_ns = api.namespace('oauth', description='oauth namespace', path='/oauth')


def redirect_js(redirect_url):
    return 'redirecting back to ui... <script>window.location = "{url}";</script>'.format(url=redirect_url)


def redirect_js_response(redirect_url, message_if_none=None, message_if_invalid=None):
    if redirect_url is not None:
        redirect_furl = furl(redirect_url)
        if not redirect_furl.netloc:
            return {'message': message_if_invalid or 'invalid redirect uri'}, 200
        return flask.Response(redirect_js(redirect_furl.url))
    else:
        return {'message': message_if_none or 'no redirect_uri provided'}, 200


# noinspection PyMethodMayBeStatic
@auth_ns.route('/site')
class Site(flask_restplus.Resource):

    def get(self):
        return {
            "url": g.site_url
        }, 200, {"Access-Control-Allow-Credentials": 'true'}


@auth_ns.route('/initialize')
class OAuthFlowInitializer(flask_restplus.Resource):

    get_parser = auth_ns.parser()
    get_parser.add_argument('client_callback_uri', type=str, location='args', required=True)
    get_parser.add_argument('scope', type=str, location='args', required=True)
    get_parser.add_argument('state', type=str, location='args', required=False)

    @auth_ns.expect(get_parser, validate=True)
    def get(self):
        args = self.get_parser.parse_args()

        # if furl(flask.request.url).origin != furl(args.get('client_callback_uri')).origin:
        #    return {"message": "invalid callback uri origin"}, 403

        oauth_callback_endpoint_uri = api.url_for(OAuthCallback, _external=True)
        state = json.dumps({
            "client_callback_uri": args.get('client_callback_uri'),
            "client_state": args.get('state', '')
        })

        return g.vv_oauth_client.redirect_for_authorization(
            oauth_callback_endpoint_uri, state=state, scope=args.get('scope'), response_type='code')


@auth_ns.route('/oauth_callback')
@auth_ns.hide
class OAuthCallback(flask_restplus.Resource):

    get_parser = auth_ns.parser()
    get_parser.add_argument('state', type=str, location='args', required=True)
    get_parser.add_argument('code', type=str, location='args', required=True)

    @auth_ns.expect(get_parser, validate=True)
    def get(self):
        args = self.get_parser.parse_args()

        state = json.loads(args.get('state', '{}'))
        client_callback_uri = state.get('client_callback_uri')
        client_callback_furl = furl(client_callback_uri)

        oauth_callback_endpoint_uri = api.url_for(OAuthCallback, _external=True)

        auth_code = g.vv_oauth_client.extract_auth_code()
        try:
            access_token_response = g.vv_oauth_client.exchange_code_for_access_token(
                auth_code, oauth_callback_endpoint_uri)
        except HTTPError:
            client_callback_furl.args['error'] = 1
            return redirect_js_response(client_callback_furl.url)

        access_token = g.vv_oauth_client.extract_access_token_from_response(access_token_response)
        issued_at = time.time()

        user = requests.get(
            os.path.join(g.site_url, 'accounts/v1/me'),
            headers={"Authorization": "Bearer {}".format(access_token)}).json()
        flask.session.update({
            "site_url": g.site_url,
            "atr": access_token_response,
            "issued_at": issued_at,
            "user_id": user['_id'],
            "email": user['email']
        })
        flask.session.modified = True

        client_callback_furl.set({
            "access_token": access_token, "site_url": g.site_url, "issued_at": issued_at,
            "user_id": user['_id'], "state": state.get('client_state'),
            "expires_in": access_token_response['expires_in'],
        })
        return redirect_js_response(client_callback_furl.url)


# noinspection PyMethodMayBeStatic
@auth_ns.route('/access_token')
class AccessToken(flask_restplus.Resource):

    def get(self):
        atr = flask.session.get('atr')
        if not atr:
            return {"message": "not authorized"}, 401, {"Access-Control-Allow-Credentials": 'true'}

        issued_at = flask.session['issued_at']
        expires_in = atr['expires_in']
        current = time.time()

        if current + 1800 < issued_at + expires_in:
            return {
                "access_token": atr['access_token'],
                "expires_in": issued_at + expires_in - current,
                "site_url": g.site_url,
            }, 200, {"Access-Control-Allow-Credentials": 'true'}

        refresh_token = flask.session['refresh_token']
        atr = g.vv_oauth_client.refresh_access_token(refresh_token)
        flask.session.update({
            "atr": atr,
            "issued_at": current,
        })
        flask.session.modified = True

        return {
            "access_token": atr['access_token'],
            "expires_in": atr['expires_in'],
            "site_url": g.site_url,
        }, 200, {"Access-Control-Allow-Credentials": 'true'}


@auth_ns.route('/logout')
class Logout(flask_restplus.Resource):

    def get(self):
        keys = ('atr', 'issued_at', 'user_id', 'email')
        for key in keys:
            flask.session.pop(key, None)
        flask.session.modified = True

        return {
            "message": 'successfully logged out',
            "site_url": g.site_url,
        }, 200


@auth_ns.route('/me')
class Me(flask_restplus.Resource):

    def get(self):
        atr = flask.session.get('atr')
        if not atr:
            return {"site_url": g.site_url}, 200, {"Access-Control-Allow-Credentials": 'true'}

        issued_at = flask.session['issued_at']
        expires_in = atr['expires_in']
        current = time.time()

        if current + 1800 < issued_at + expires_in:
            return {
                "access_token": atr['access_token'],
                "expires_in": issued_at + expires_in - current,
                "site_url": g.site_url,
            }, 200, {"Access-Control-Allow-Credentials": 'true'}

        refresh_token = flask.session['refresh_token']
        atr = g.vv_oauth_client.refresh_access_token(refresh_token)
        flask.session.update({
            "atr": atr,
            "issued_at": current,
        })
        flask.session.modified = True

        return {
            "access_token": atr['access_token'],
            "expires_in": atr['expires_in'],
            "site_url": g.site_url,
        }, 200, {"Access-Control-Allow-Credentials": 'true'}
