import functools
import json
import os

from flask import g, current_app
from ..oauth_helper import VedavaapiOAuthClient

(dir_path, fname) = os.path.split(__file__)
root_dir = os.path.normpath(os.path.join(dir_path, '../../'))
default_site_config_path = os.path.join(root_dir, 'site_config.json')


def push_environ_to_g():
    site_config_path = current_app.config.get('SITE_CONFIG_PATH', default_site_config_path)
    g.site_config = json.loads(open(site_config_path, 'rb').read().decode('utf-8'))
    g.site_url = g.site_config['site_url']
    g.vv_oauth_client = VedavaapiOAuthClient(
            g.site_url, g.site_config['client_id'], g.site_config['client_secret'])


def set_environ(func):
    @functools.wraps(func)
    def _with_environ(*args, **kwargs):
        push_environ_to_g()
        return func(*args, **kwargs)
    return _with_environ
