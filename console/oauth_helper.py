import logging
import os

import flask
import requests
from furl import furl

logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)s: %(asctime)s {%(filename)s:%(lineno)d}: %(message)s "
)


class VedavaapiOAuthClient(object):
    provider_name = 'vedavaapi'

    def __init__(self, site_url, client_id, client_secret):
        super(VedavaapiOAuthClient, self).__init__()
        self.site_url = site_url
        self.client_id = client_id
        self.client_secret = client_secret

        self.auth_url = os.path.join(self.site_url, 'accounts/v1/oauth/authorize')
        self.token_url = os.path.join(self.site_url, 'accounts/v1/oauth/token')

    # step 1 on server side
    def redirect_for_authorization(self, redirect_uri, state, scope=None, response_type=None):
        params = {
            'response_type': response_type or 'code',
            'client_id': self.client_id,
            'redirect_uri': redirect_uri,
            'scope': scope or 'vedavaapi.root',
            'state': state,
        }

        auth_furl = furl(self.auth_url)
        auth_furl.args.update(params)
        return flask.redirect(auth_furl.url)

    # noinspection PyMethodMayBeStatic
    def extract_auth_code(self):
        return flask.request.args.get('code')

    # step2 on server side
    def exchange_code_for_access_token(self, auth_code, registered_callback_url):
        request_data = {
            'grant_type': 'authorization_code',
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'redirect_uri': registered_callback_url,
            'code': auth_code,
        }

        atr = requests.post(self.token_url, data=request_data)
        atr.raise_for_status()
        return atr.json()

    def refresh_access_token(self, refresh_token):
        request_data = {
            'grant_type': 'refresh_token',
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            "refresh_token": refresh_token,
        }
        atr = requests.post(self.token_url, data=request_data)
        atr.raise_for_status()
        return atr.json()

    # noinspection PyMethodMayBeStatic
    def extract_access_token_from_response(self, atr):
        if not atr:
            return None
        return atr.get('access_token', None)

    # noinspection PyMethodMayBeStatic
    def extract_refresh_token_from_response(self, atr):
        if not atr:
            return None
        return atr.get('refresh_token', None)
