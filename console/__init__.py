import json
import os

import flask
from flask_cors import CORS

app = flask.Flask(__name__, instance_relative_config=True)
CORS(app)


def bootstrap_instance_config():
    (dir_path, fname) = os.path.split(__file__)
    instance_dir = os.path.normpath(os.path.join(dir_path, '../instance'))
    if not os.path.isdir(instance_dir):
        os.makedirs(instance_dir, exist_ok=True)

    instance_config_path = os.path.join(instance_dir, 'config.json')
    if not os.path.exists(instance_config_path):
        from base64 import b64encode
        secret_key = b64encode(os.urandom(24)).decode('utf-8')

        instance_config = {"SECRET_KEY": secret_key}
        open(os.path.join(instance_dir, 'config.json'), 'wb').write(json.dumps(instance_config).encode('utf-8'))

    app.config.from_json(filename='config.json')


try:
    bootstrap_instance_config()
except FileNotFoundError as e:
    raise e

from .api import api_blueprint_v1
app.register_blueprint(api_blueprint_v1, url_prefix='')
