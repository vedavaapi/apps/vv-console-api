import os
import sys
from werkzeug.debug import DebuggedApplication

(app_path, fname) = os.path.split(__file__)
if app_path:
    os.chdir(app_path)
sys.path.insert(0, app_path)


from console import app

application = DebuggedApplication(app)


if __name__ == '__main__':
    port = sys.argv[1] if len(sys.argv) > 1 else 5000
    app.run(host='0.0.0.0', port=port)
